/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visualize;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JPanel;
import org.apache.commons.math3.stat.clustering.Cluster;
import wordnet.Word;

/**
 *
 * @author Mateusz
 */
 public class CellPane extends JPanel {

    private Color defaultBackground;
    private Cluster<Word> cluster;
    private Word hyponim;
    private final int cellWidth;
    private final int cellHeight;
    private boolean isSelected = false;
    private boolean isDoubleClick = false;
    
    public CellPane(Word hyponim, int cellWidth, int cellHeight) {
        this.hyponim = hyponim; 
        this.cellWidth = cellWidth;
        this.cellHeight = cellHeight;
        
        this.setMinimumSize(new Dimension(this.cellWidth, this.cellHeight));
        Label name = new Label(hyponim.getData());
        
        name.setAlignment(Label.CENTER);
        this.add(name);
        
        Label size = new Label("size: " + hyponim.getHyponyms().size());
        name.setAlignment(Label.CENTER);
        this.add(size);
        Listener();
    }
     
    public CellPane(Cluster<Word> cluster, int cellWidth, int cellHeight) {
        this.cluster = cluster;
        this.cellWidth = cellWidth;
        this.cellHeight = cellHeight;
        
        this.setMinimumSize(new Dimension(this.cellWidth, this.cellHeight)); 
        List<Word> points = cluster.getPoints();
        
        Label name = new Label(points.get(0).getData());
        name.setAlignment(Label.CENTER);
        this.add(name);
                
        Label size = new Label("size: " + points.size());
        name.setAlignment(Label.CENTER);
        this.add(size);
        
        Listener();
    }
    
    public Word getWord() {
        return hyponim;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(this.cellWidth, this.cellHeight);
    }
    
    public boolean isDoubleClick() {
        return isDoubleClick;
    }
    
    public boolean isSelected() {
        return isSelected;
    }
    
    private void Listener() {        
        defaultBackground = getBackground();
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                    setBackground(Color.BLUE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                    setBackground(defaultBackground);
                    isSelected = false;
            }

            @Override
            public void mousePressed(MouseEvent e) {
                    setBackground(Color.RED);
                    if (isSelected) {
                        isDoubleClick = true;
                    }
                    isSelected = true;
            }
        });
    }
 }