/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visualize;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import org.apache.commons.math3.stat.clustering.Cluster;
import wordnet.Word;

/**
 *
 * @author Mateusz
 */
public class GridCluster extends JPanel {

    private int elements;
    private int cellSizeWidth;
    
    private CellPane[] cellPane;
    
    public GridCluster(ArrayList<Word> TreeRoot, int width ,int cellSizeWidth, int cellSizeHeight) {
        setLayout(new GridBagLayout());
        this.elements = TreeRoot.size();   
        this.cellSizeWidth = cellSizeWidth;
        
        InitGridCluster(TreeRoot, cellSizeWidth, cellSizeHeight);
        RePaintGridCluster(width, this.cellSizeWidth);
    }
    
    public GridCluster(List<Cluster<Word>> clusterList, int width ,int cellSizeWidth, int cellSizeHeight) {
        setLayout(new GridBagLayout());
        this.elements = clusterList.size();
        this.cellSizeWidth = cellSizeWidth;
        
        InitGridCluster(clusterList, cellSizeWidth, cellSizeHeight);
        RePaintGridCluster(width, this.cellSizeWidth);
    }

    void Listener() {
            addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                    setBackground(Color.RED);
            }
        });
    }
    public void GridClusterResized(int width) {
        RePaintGridCluster(width, this.cellSizeWidth);
    }

    public void GridClusterResized(int width, int cellSize) {
        RePaintGridCluster(width, cellSize);
    }

    private void RePaintGridCluster(int width, int cellSizeWidth) {
        this.removeAll();
        int maxCol = (width - 1) / cellSizeWidth;
        GridBagConstraints gbc = new GridBagConstraints();

        for (int i = 0; i < this.elements;) {
            for (int col = 0; col < maxCol; ++col, ++i) {
                if (i >= this.elements) {
                    break;
                }

                gbc.gridx = col;
                gbc.gridy = i / maxCol;
                this.add(cellPane[i], gbc);
            }
        }
    }
    
    public CellPane getSelected() {
        for (int i = 0; i < this.cellPane.length; ++i) {
            if (cellPane[i].isSelected()) {
                return cellPane[i];
            }
        }
        return null;
    }
    
    private void InitGridCluster(List<Cluster<Word>> clusterList, int cellSizeWidth, int cellSizeHeight) {
        cellPane = new CellPane[this.elements];
        for (int i = 0; i < this.elements; ++i) {
            cellPane[i] = new CellPane(clusterList.get(i), cellSizeWidth, cellSizeHeight);
            Border border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
            cellPane[i].setBorder(border);
        }
    }

    private void InitGridCluster(ArrayList<Word> TreeRoot, int cellSizeWidth, int cellSizeHeight) {
        cellPane = new CellPane[this.elements];
        for (int i = 0; i < this.elements; ++i) {
            cellPane[i] = new CellPane(TreeRoot.get(i), cellSizeWidth, cellSizeHeight);
            Border border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
            cellPane[i].setBorder(border);
        }
    }
}
