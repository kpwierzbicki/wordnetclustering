package wordnet;

import edu.smu.tspell.wordnet.NounSynset;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import org.apache.commons.math3.stat.clustering.Clusterable;

/**
 *
 * @author Mateusz
 */
public class Word implements Clusterable<Word>{
   
    private String description;
    private String data;
    private WordType type;
    private List<Word>hypernyms;
    private List<Word> hyponyms;
    private Word hypernym;
    public String getDescription() {
        return description;
    }

    public String getData() {
        return data;
    }

    public WordType getType() {
        return type;
    }
    
    public List<Word> getHypernyms() {
        return hypernyms;
    }
    public void setHypernyms(List<Word> hypernyms) {
        this.hypernyms = hypernyms ;
    }
    
    public Word getHypernym() {
        return hypernym;
    }
    public void setHypernym(Word hypernym) {
        this.hypernym =hypernym ;
    }
    
    public void setHyponyms(List<Word> hyponyms) {
        this.hyponyms =hyponyms ;
    }
    public List<Word> getHyponyms() {
        return hyponyms;
    }
    public Word(String data, WordType type, String des) {
        this(data, type);
        this.description = des;
    }
    
    public Word(String data, WordType type) {
        this.data = data;
        this.type = type;
    }
    
    @Override
    public String toString() {
        return data;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.data);
        hash = 13 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Word other = (Word) obj;
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public double distanceFrom(Word t) {
        return 0;
    }

    @Override
    public Word centroidOf(Collection<Word> clctn) {
        return this;
    }

    
}
