package wordnet;

import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.WordNetDatabase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Mateusz
 */
public class WordnetDB {

    private WordNetDatabase wordnetDB;
    private String pathDB;

    private Collection<Word> allNouns;
    private Collection<Word> allAdjectives;
    private Collection<Word> allVerbs;
    private Collection<Word> allAdverbs;

    private Set<Word> uniqueNouns;
    private Set<Word> uniqueAdjectives;
    private Set<Word> uniqueVerbs;
    private Set<Word> uniqueAdverbs;

    /**
     * Constructor Set up default path to wordnet db
     */
    public WordnetDB() {
        this("C:\\WordNet-3.0\\dict\\");
    }

    /**
     * Constructor
     *
     * @param path - path to WordNet database
     */
    public WordnetDB(String pathDataBase) {
        this.pathDB = pathDataBase + "\\";
        System.setProperty("wordnet.database.dir", this.pathDB);
        this.wordnetDB = WordNetDatabase.getFileInstance();
        
        if (IsDBLoad()) {
            // Read files from wordnet, note that may occur duplicate the same word
            // but with different description
            allNouns = WordnetFile.Read(pathDB, WordType.NOUN);
            allVerbs = WordnetFile.Read(pathDB, WordType.VERB);
            allAdverbs = WordnetFile.Read(pathDB, WordType.ADVERB);
            allAdjectives = WordnetFile.Read(pathDB, WordType.ADJECTIVE);

            // remove duplicates 
            uniqueNouns = removeDuplicates(allNouns);
            uniqueVerbs = removeDuplicates(allVerbs);
            uniqueAdverbs = removeDuplicates(allAdverbs);
            uniqueAdjectives = removeDuplicates(allAdjectives);
        }
    }

    public boolean IsDBLoad() {
        try {
            this.wordnetDB.getSynsets("dog");
            return true;
        } catch(Exception e) {
            return false;
        }
    }

/*
 * @ return - all synonyms from all synsets
 */
public Collection<Word> getAllWordForms(Word word) {
        Synset[] synsets = wordnetDB.getSynsets(word.toString(), WordType.getSynsetType(word.getType()));
        List<Word> list = new ArrayList();

        for (Synset synset : synsets) {
            String[] wordForms = synset.getWordForms();
            for (String wordForm : wordForms) {
                list.add(new Word(wordForm, word.getType(), synset.getDefinition()));
            }
        }
        return list;
    }

    /*
     * @ return - unique synonyms from all synsets
     */
    public Collection<Word> getUniqueWordForms(Word word) {
        Collection<Word> list = getAllWordForms(word);
        return removeDuplicates(list);
    }

    /*
     * @ return - synonyms from synset that contains the word in the same sense
     */
    public Collection<Word> getSynonyms(Word word) {
        Synset[] synsets = wordnetDB.getSynsets(word.toString(), WordType.getSynsetType(word.getType()));
        List<Word> list = new ArrayList();
        for (Synset synset : synsets) {
            String[] wordForms = synset.getWordForms();
            if (word.getDescription().contains(synset.getDefinition())) {
                for (String wordForm : wordForms) {
                    list.add(new Word(wordForm, word.getType(), synset.getDefinition()));
                }
                return list;
            }
        }
        return list;
    }

    public List<Word> getHypernyms(Word word) {
        Synset[] synsets = wordnetDB.getSynsets(word.toString(), WordType.getSynsetType(word.getType()));
        List<Word> hyponyms = new ArrayList();
        for (Synset synset : synsets) {
            if (word.getDescription().contains(synset.getDefinition())) {
                for (NounSynset nSynset : ((NounSynset) synset).getHypernyms()) {
                    for (String wordForm : nSynset.getWordForms()) {
                        hyponyms.add(new Word(wordForm, word.getType(), nSynset.getDefinition()));
                    }
                }
            }
        }
        return hyponyms;
    }

    public List<Word> getHyponyms(Word word) {
        Synset[] synsets = wordnetDB.getSynsets(word.toString(), WordType.getSynsetType(word.getType()));
        List<Word> hyponyms = new ArrayList();
        for (Synset synset : synsets) {
            if (word.getDescription().contains(synset.getDefinition())) {
                for (NounSynset nSynset : ((NounSynset) synset).getHyponyms()) {
                    for (String wordForm : nSynset.getWordForms()) {
                        Word newChild = new Word(wordForm, word.getType(), nSynset.getDefinition());
                        newChild.setHypernym(word);
                        hyponyms.add(newChild);
                    }
                }
            }
        }
        return hyponyms;
    }

    //R
    public Collection<Word> getSynsetsInWord(Word word) {
        Synset[] synsets = wordnetDB.getSynsets(word.toString(), WordType.getSynsetType(word.getType()));
        List<Word> list = new ArrayList();
        for (Synset synset : synsets) {
            String[] wordForms = synset.getWordForms();
            for (String wordForm : wordForms) {
                list.add(new Word(wordForm, word.getType(), synset.getDefinition()));
            }
        }
        return list;
    }

    public Collection<Word> getAllNouns() {
        return this.allNouns;
    }

    public Collection<Word> getAllVerbs() {
        return this.allVerbs;
    }

    public Collection<Word> getAllAdverbs() {
        return this.allAdverbs;
    }

    public Collection<Word> getAdjectives() {
        return this.allAdjectives;
    }

    public Set<Word> getUniqueNouns() {
        return this.uniqueNouns;
    }

    public Set<Word> getUniqueVerbs() {
        return this.uniqueVerbs;
    }

    public Set<Word> getUniqueAdverbs() {
        return this.uniqueAdverbs;
    }

    public Set<Word> getUniqueAdjectives() {
        return this.uniqueAdjectives;
    }

    private Set<Word> removeDuplicates(Collection<Word> items) {
        Set<Word> unique = new HashSet();
        for (Word item : items) {
            unique.add(item);
        }
        return unique;
    }
}
