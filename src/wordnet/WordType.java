package wordnet;

import edu.smu.tspell.wordnet.SynsetType;

/**
 *
 * @author Mateusz
 */
public enum WordType {
    NOUN,
    VERB,
    ADVERB,
    ADJECTIVE;
    
    public static SynsetType getSynsetType(WordType type) {
        switch (type) {
            case NOUN:
                return SynsetType.NOUN;
            case VERB:
                return SynsetType.VERB;
            case ADVERB:
                return SynsetType.ADVERB;
            case ADJECTIVE:
                return SynsetType.ADJECTIVE;
        }
        return null;
    } 
    
    public static String getNameFile(WordType type) {
        switch (type) {
            case NOUN:
                return "data.noun";
            case VERB:
                return "data.verb";
            case ADVERB:
                return "data.adv";
            case ADJECTIVE:
                return "data.adj";
        }
        return "";
    }
}
