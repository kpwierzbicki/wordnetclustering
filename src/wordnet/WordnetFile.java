package wordnet;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Mateusz
 */
public class WordnetFile {

    private static String LastReadFile = "";
    private static List<Word> dataFile = null;
    
    private static final String VBAR = "|";
    private static final String SPACE = " ";
    private static final String SEMICOLON = ";";
    private static final String UNDER_SCORE = "_";

    /*
     * @return - Collection of words from file
     */
    public static Collection<Word> Read(String dataPath, WordType type) {
        String fileName = dataPath + WordType.getNameFile(type);

        // return data if is current 
        if (fileName.equals(LastReadFile) && dataFile != null) {
            return dataFile;
        }

        try {
            BufferedReader bufReader = new BufferedReader(new FileReader(fileName));
            dataFile = new ArrayList<>();
            String line;

            while ((line = bufReader.readLine()) != null) {
                // don't read initial comment
                if (line.charAt(0) != ' ') {
                    String splitLine[] = line.split(SPACE, 6);
                    String data = splitLine[4].replace(UNDER_SCORE, SPACE);

                    int beginIndex = splitLine[5].indexOf(VBAR) + 1;
                    int endIndex = splitLine[5].indexOf(SEMICOLON);
                    if (endIndex < beginIndex) {
                        endIndex = splitLine[5].length();
                    }
                    
                    String des = splitLine[5].substring(beginIndex, endIndex);
                    dataFile.add(new Word(data, type, des));
                }
            }
        } catch (FileNotFoundException ex) {
            System.err.println("File can't found " + fileName + " error msg: " + ex.getMessage());
        } catch (IOException ex) {
            System.err.println("IO exception " + fileName + " error msg: " + ex.getMessage());
        }

        LastReadFile = fileName;
        return dataFile;
    }
}
