package clustering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.stat.clustering.Cluster;
import wordnet.Word;
import wordnet.WordnetDB;

/**
 *
 * @author Mateusz
 */
public class WordnetClustering {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        WordnetDB db = new WordnetDB();
        Collection<Word> ok = db.getAllNouns();
        int stoper = 0; 
        
        //TreeClustering(ok, db);
        
        TreeClusteringOnlyRoots(ok,db);
        
        //DBScanning(ok, db);
        
        System.out.println("Ending");
    }

    private static void DBScanning(Collection<Word> ok, WordnetDB db)
    {
        int count = 0, countNull = 0;
        for (Word item : ok) {
            item.setHypernyms(db.getHypernyms(item));
            if(item.getHypernyms().isEmpty())
                countNull++;
            count++;
            item.setHyponyms(db.getHyponyms(item));
        }
        MyDBScan dbscan = new MyDBScan(1, 1);
        List<Cluster<Word>> clusterList = dbscan.cluster(ok);
        for (Cluster<Word> item : clusterList) {
            //System.out.println(item);
            for(Word i :item.getPoints())
            {
                System.out.println(Integer.toString(item.getPoints().size())+ "    "+ i.getHypernyms().get(0).getData());
                break;
            }
        }
    }
    

    private static void TreeClustering(Collection<Word> ok, WordnetDB db) 
    {
        List<Word> TreeRoot = new ArrayList();
        for (Word item : ok) {
            if (db.getHypernyms(item).isEmpty()&& !db.getHyponyms(item).isEmpty())
            {
                TreeRoot.add(item);
            }
        }
        for (Word item : TreeRoot)
        {
            FindAnsester(item, db);
        }
        System.out.println("TreeClustering ending");
    }
    private static void FindAnsester(Word word, WordnetDB db)
    {
        word.setHyponyms(db.getHyponyms(word));
        counting++;
        for(Word item : word.getHyponyms())
        {
            item.setHypernym(word);
            FindAnsester(item,db);
        }
    }
    private static int counting = 0;
    private static void TreeClusteringOnlyRoots(Collection<Word> ok, WordnetDB db) {
        List<Word> TreeRoot = new ArrayList();
        for (Word item : ok) {
            if (db.getHypernyms(item).isEmpty()&& !db.getHyponyms(item).isEmpty())
            {
                item.setHyponyms(db.getHyponyms(item));
                TreeRoot.add(item);
            }
        }
        
        System.out.println("TreeClustering only parents ending");
    }
}
